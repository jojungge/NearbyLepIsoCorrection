#ifndef INEARBYLEPISOCORRECTION_h
#define INEARBYLEPISOCORRECTION_h

/// @file INearbyLepIsoCorrection.h
/// @author Johannes Junggeburth ( johannes.josef.junggeburth@cern.ch )
/// @brief Defines the interface for the NearByLeptonIsolation correction tool
/// This should only be used in RootCore.

#include "AsgTools/IAsgTool.h"
#include <xAODPrimitives/IsolationType.h>

//Systematics Header
//#include "PATInterfaces/SystematicCode.h"
//#include "PATInterfaces/SystematicSet.h"
//
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"

namespace CP {
    class SystematicSet;
}

namespace NearLep {
    enum StoredCone {
        Original, Recalculated, Corrected
    };
    inline std::string PromptStoredCone(StoredCone T) {
        if (T == StoredCone::Original) return "Original";
        if (T == StoredCone::Corrected) return "Corrected";
        if (T == StoredCone::Recalculated) return "Recalculated";
        return "Unknown";
    }

    const float MiniIsoPT = 1.e4; // The pt for the PtVarCone calculation is 10 GeV
    const float CaloCoreDR2 = 0.01 * 0.01;
    class IIsoCorrection: public virtual asg::IAsgTool {
            // Declare the interface that the class provides
            ASG_TOOL_INTERFACE (NearLep::IsoCorrection)

        public:
            virtual StatusCode SaveIsolation(const xAOD::Electron* E) = 0;
            virtual StatusCode SaveIsolation(const xAOD::Muon* M) = 0;

            virtual StatusCode LoadIsolation(const xAOD::Electron* E, StoredCone T = StoredCone::Original)=0;
            virtual StatusCode LoadIsolation(const xAOD::Muon* M, StoredCone T = StoredCone::Original)=0;

            virtual StatusCode CorrectIsolation(xAOD::ElectronContainer* Electrons, xAOD::MuonContainer* Muons) = 0;

            virtual StatusCode SetSystematic(const CP::SystematicSet& Set) = 0;

            virtual double ConeSize(const xAOD::IParticle* P, xAOD::Iso::IsolationType Cone) const= 0;
            virtual float GetIsoVar(const xAOD::IParticle* P, xAOD::Iso::IsolationType Iso, StoredCone T = StoredCone::Original) const= 0;
            virtual ~IIsoCorrection() {
            }
    };
}
#endif // #ifdef INEARBYLEPISOCORRECTION_h

