#include "GaudiKernel/DeclareFactoryEntries.h"

#include <NearbyLepIsoCorrection/NearbyLepIsoCorrection.h>

DECLARE_NAMESPACE_TOOL_FACTORY(NearLep, IsoCorrection)


DECLARE_FACTORY_ENTRIES(NearbyLepIsoCorrection) {
    DECLARE_NAMESPACE_TOOL(NearLep, IsoCorrection)
  }
